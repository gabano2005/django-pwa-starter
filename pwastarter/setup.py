import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__),'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-pwastarter',
    version='0.0.1-rc2',
    packages=find_packages(exclude='pwastarter'),
    include_package_data=True,
    license='BSD License',
    description='Package provides base endpoints for a PWA',
    long_description=README,
    url='gitlab.com',
    author='Nana Duah',
    install_requires=['django>=2.1,<3']
)