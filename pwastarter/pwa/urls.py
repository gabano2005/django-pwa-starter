from django.urls import path
from . import views

appName = "pwastarter"
urlpatterns = [
    path('sw.js', view=views.serviceWorkerJs, name='swJs'),
    path('manifest.json', view=views.manifestFile, name='manifestJson'),
]