from django.test import TestCase

class PwaStarterTests(TestCase):
    def test_that_service_wroker_endpoint_services_js(self):
        response = self.client.get('/sw.js')
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['content-type'],'text/javascript')

    def test_that_manifest_endpoint_returns_json(self):
        response = self.client.get('/manifest.json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['content-type'],'application/json')