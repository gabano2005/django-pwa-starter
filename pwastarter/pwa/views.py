from io import open
from django.shortcuts import render
from django.http.response import HttpResponse
from django.conf import settings

BASE_DIR = getattr(settings,'BASE_DIR')
def serviceWorkerJs(request):
    serviceWorkerPath='{base}/static/sw.js'.format(base=BASE_DIR)
    content_type = 'text/javascript'
    with open(serviceWorkerPath,'rb') as sw:
        response = HttpResponse(content=sw, content_type=content_type)
        response['Cache-Control']='no-cache'
        return response

def manifestFile(request):
    manifestFilePath = '{base}/manifest.json'.format(base=BASE_DIR)
    content_type = 'application/json'
    with open(manifestFilePath, 'rb') as manifest:
        response= HttpResponse(content=manifest, content_type=content_type)
        return response


